#!/usr/bin/env python

import os,sys
import json
import argparse
import base64
import boto3
from bson import json_util

def main():
     '''main'''
     parser = argparse.ArgumentParser(description=main.__doc__)
     parser.add_argument("-u", "--userdata", required=True, help="user data file, name will be used as launch template name\n")
     parser.add_argument("-d", "--delete", required=False, default=False, action="store_true", help="will delete launch template first\n")
     parser.add_argument("-b", "--basic", required=False, default=True, action="store_true", help="will create a baisic non-cromwell template\n")          
     args = parser.parse_args()

     client = boto3.client('ec2')
     if args.delete:
          response = client.delete_launch_template(LaunchTemplateName=args.userdata)
          print(json.dumps(response, indent=3, default=json_util.default))
     with open(args.userdata, "r") as f: ud = f.read()
     ud_base64 = base64.b64encode(ud.encode('utf-8')).decode('utf-8')
     template = json.loads(get_template_skeleton())
     if args.basic:
          template = json.loads(get_template_skeleton_basic())
     else:
          template = json.loads(get_template_cromwell_wf())
          
     template['LaunchTemplateName']=args.userdata
     template['LaunchTemplateData']['UserData']=ud_base64
     response = client.create_launch_template(LaunchTemplateName=template["LaunchTemplateName"],LaunchTemplateData=template["LaunchTemplateData"])
     print(json.dumps(response, indent=3, default=json_util.default))     
#     print (json.dumps(template,indent=3))
     return


def get_template_cromwell_wf():
    skel='''
        {
            "LaunchTemplateName": "{{lt-name}}",
            "LaunchTemplateData": {
                "BlockDeviceMappings": [
                    {
                        "DeviceName": "/dev/xvda",
                        "Ebs": {
                            "DeleteOnTermination": true,
                            "VolumeSize": 50,
                            "VolumeType": "gp2"
                        }
                    },
                    {
                        "DeviceName": "/dev/xvdcz",
                        "Ebs": {
                            "Encrypted": true,
                            "DeleteOnTermination": true,
                            "VolumeSize": 75,
                            "VolumeType": "gp2"
                        }
                    },
                    {
                        "DeviceName": "/dev/sdc",
                        "Ebs": {
                            "Encrypted": true,
                            "DeleteOnTermination": true,
                            "VolumeSize": 8,
                            "VolumeType": "gp2"
                        }
                    }
                ],
                "UserData": "TUlNRS1WZXJzaW9uOiAxLjAKQ29udGVudC1UeXBlOiBtdWx0aXBhcnQvbWl4ZWQ7IGJvdW5kYXJ5PSI9PUJPVU5EQVJZPT0iCgotLT09Qk9VTkRBUlk9PQpDb250ZW50LVR5cGU6IHRleHQvY2xvdWQtY29uZmlnOyBjaGFyc2V0PSJ1cy1hc2NpaSIKCnBhY2thZ2VzOgotIGpxCi0gYnRyZnMtcHJvZ3MKLSBweXRob24yNy1waXAKLSBzZWQKLSB3Z2V0Ci0gcGVybAoKcnVuY21kOgotIHBpcCBpbnN0YWxsIC1VIGF3c2NsaSBib3RvMwotIHNjcmF0Y2hQYXRoPSIvY3JvbXdlbGxfcm9vdCIKLSBhcnRpZmFjdFJvb3RVcmw9Imh0dHBzOi8vczMuYW1hem9uYXdzLmNvbS9hd3MtZ2Vub21pY3Mtd29ya2Zsb3dzL2FydGlmYWN0cyIKLSBjZCAvb3B0ICYmIHdnZXQgJGFydGlmYWN0Um9vdFVybC9hd3MtZWJzLWF1dG9zY2FsZS50Z3ogJiYgdGFyIC14emYgYXdzLWVicy1hdXRvc2NhbGUudGd6Ci0gc2ggL29wdC9lYnMtYXV0b3NjYWxlL2Jpbi9pbml0LWVicy1hdXRvc2NhbGUuc2ggJHNjcmF0Y2hQYXRoIC9kZXYvc2RjICAyPiYxID4gL3Zhci9sb2cvaW5pdC1lYnMtYXV0b3NjYWxlLmxvZwotIGNkIC9vcHQgJiYgd2dldCAkYXJ0aWZhY3RSb290VXJsL2F3cy1lY3MtYWRkaXRpb25zLnRneiAmJiB0YXIgLXh6ZiBhd3MtZWNzLWFkZGl0aW9ucy50Z3oKLSBzaCAvb3B0L2Vjcy1hZGRpdGlvbnMvZWNzLWFkZGl0aW9ucy1jcm9td2VsbC5zaAotIGVjaG8gInNldHRpbmcgc3lzdGVtIHVsaW1pdHMiCi0gZWNobyAtZSAiI2NvbW1hbmRzXG5yb290IHNvZnQgbm9maWxlIDY1NTM2XG5yb290IGhhcmQgbm9maWxlIDY1NTM2XG4qIHNvZnQgbm9maWxlIDY1NTM2XG4qIGhhcmQgbm9maWxlIDY1NTM2IiA+PiAvZXRjL3NlY3VyaXR5L2xpbWl0cy5jb25mCi0gc3lzY3RsIC13IGZzLmZpbGUtbWF4PTY0MDAwCi0gdWxpbWl0IC1uCi0gdWxpbWl0IC1IbgotIGVjaG8gInNldHRpbmcgZG9ja2VyIHVsaW1pdHMiCi0gc2VydmljZSBkb2NrZXIgc3RvcAotIHBlcmwgLWkuYmFrIC1wbmUgInMvZmlsZT0uKlxcXCIvZmlsZT02MDAwMDo2NTAwMFxcXCIvIiAvZXRjL3N5c2NvbmZpZy9kb2NrZXIKLSBzZXJ2aWNlIGRvY2tlciBzdGFydAotIGVjaG8gImdldHRpbmcgYW5kIHN0YXJ0aW5nIGVicy1jbGVhbnVwIgotIGNkIC9vcHQgJiYgd2dldCAtcU8gLSBodHRwczovL2JpdGJ1Y2tldC5vcmcvYmVya2VsZXlsYWIvamdpLW1ldGEvZ2V0L21hc3Rlci50YXIuZ3ogfCB0YXIgLS13aWxkY2FyZHMgLXp4dmYgLSAiKi9iaW4vZWJzX2NsZWFudXAucHkiICIqL2Jpbi9yZXNvdXJjZXMuYmFzaCIgfCAgeGFyZ3MgLWkgbXYge30gLgotIC9vcHQvZWJzX2NsZWFudXAucHkgIDI+JjEgPiAvY3JvbXdlbGxfcm9vdC9lYnMtY2xlYW51cC5sb2cgJgotIC9vcHQvcmVzb3VyY2VzLmJhc2ggIDI+JjEgPiAvY3JvbXdlbGxfcm9vdC9yZXNvdXJjZXMubG9nICYKLSBlY2hvICJkb25lIHdpdGggc2V0dXAiCgotLT09Qk9VTkRBUlk9PS0tCg==",
                "TagSpecifications": [
                    {
                        "ResourceType": "instance",
                        "Tags": [
                            {
                                "Key": "architecture",
                                "Value": "genomics-workflows"
                            },
                            {
                                "Key": "solution",
                                "Value": "cromwell"
                            }
                        ]
                    },
                    {
                        "ResourceType": "volume",
                        "Tags": [
                            {
                                "Key": "architecture",
                                "Value": "genomics-workflows"
                            },
                            {
                                "Key": "solution",
                                "Value": "cromwell"
                            }
                        ]
                    }
                ]
            }
        }
    '''
    return skel

def get_template_skeleton_basic():
    skel='''
        {
            "LaunchTemplateName": "{{lt-name}}",
            "LaunchTemplateData": {
                "BlockDeviceMappings": [
                    {
                        "DeviceName": "/dev/xvda",
                        "Ebs": {
                            "DeleteOnTermination": true,
                            "VolumeSize": 50,
                            "VolumeType": "gp2"
                        }
                    }
                ],
                "UserData": ""
            }
        }
    '''
    return skel

if __name__=="__main__":
    main()
    
