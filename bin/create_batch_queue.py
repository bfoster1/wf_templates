#!/usr/bin/env python

import os, sys
import argparse
import re

def main():
    '''Takes type (SPOT, EC2) name, instance and create 2 json files and commands to delete and create queus and cromwell.conf'''    
    parser = argparse.ArgumentParser(description=main.__doc__)
#    parser.add_argument('inputfile', help="input json file operate on")
    parser.add_argument("-n", "--name", required=True, default="debug", help="q name\n")
    parser.add_argument("-p", "--provision", required=False, choices=['SPOT','EC2'], default="SPOT", help="provisioning type\n")
    parser.add_argument("-i", "--instance", required=False, default="r5.4xlarge", help="instance type\n")
    parser.add_argument("-l", "--launch_template", required=False, default="lt-0b37ae56763bc019b", help="launchtemplate type\n")
    parser.add_argument("-o", "--outdir", required=False, default="./", help=" output dir (./ default)\n")    

    args = parser.parse_args()

    name = args.name
    if not name.endswith("-ce"):
        name += "-ce"
        
    #compute environment
    ce = get_ce_template(provision=args.provision)
    ce= re.sub(r'{{name}}',name, ce, flags=re.MULTILINE)
    instance = "\"" + "\"\,\".".join(args.instance.split(',')) + "\""
    ce = re.sub(r'{{instance}}',instance, ce, flags=re.MULTILINE)
    ce = re.sub(r'{{lt}}',args.launch_template, ce, flags=re.MULTILINE)    
    #queue
    queue_name = name + "q"
    queue = get_ceq_template()
    queue = re.sub(r'{{qname}}',queue_name, queue, flags=re.MULTILINE)
    queue = re.sub(r'{{name}}',name, queue, flags=re.MULTILINE)

    
    #cromwell stub
    cromwell = get_cromwell_template()
    cromwell = re.sub(r'{{qname}}',queue_name, cromwell, flags=re.MULTILINE)

    ce_file = os.path.join(args.outdir, name + ".json")
    with open(ce_file, "w") as f:f.write(ce + "\n")

    q_file = os.path.join(args.outdir, queue_name + ".json")
    with open(q_file, "w") as f:f.write(queue + "\n")

    cw_file = os.path.join(args.outdir, name + ".conf")
    with open(cw_file, "w") as f:f.write(cromwell + "\n")
    
    cmd = list()
    cmd.append("#aws batch update-job-queue --state=disabled --job-queue=" + queue_name + "\n")
    cmd.append("#aws batch update-compute-environment --state=disabled --compute-environment=" + name + "\n")

    cmd.append("#watch \"aws batch delete-job-queue --job-queue=" + queue_name + "\"\n")
    cmd.append("#watch \"aws batch delete-compute-environment --compute-environment=" + name + "\"\n\n")

    cmd.append("aws batch create-compute-environment --cli-input-json file://" + ce_file + "\n")
    cmd.append("aws batch create-job-queue --cli-input-json file://" + q_file + "\n" ) 



    cmd_file = os.path.join(args.outdir, name + ".cmds")
    with open(cmd_file, "w") as f:f.write("".join(cmd) +  "\n")
    return


def get_ce_template(provision="SPOT"):
    '''returns template'''
    template='''
{
   "computeEnvironmentName": "{{name}}",
   "serviceRole": "arn:aws:iam::080819234838:role/bf-20190603-uswest2-iam-GenomicsEnvBatchServiceRol-12FCNMAOJ4DVP",
   "type": "MANAGED",
   "state": "ENABLED",
   "computeResources": {
       "tags": {"Name":"{{name}}"},
      "ec2KeyPair": "bf-20190528-west2",
      "launchTemplate": {
         "launchTemplateId": "{{lt}}",
         "version": "$Default"
      },
      "instanceRole": "arn:aws:iam::080819234838:instance-profile/bf-20190603-uswest2-iam-GenomicsEnvBatchInstanceProfile-K3S0YR4V9ZND",
      "instanceTypes": [
         {{instance}}
      ],
      "minvCpus": 0,
      "maxvCpus": 512,
      "securityGroupIds": [
         "sg-527a3836"
      ],
      {{spot-fleet}}
      "subnets": [
         "subnet-7dd8b924",
         "subnet-27002b42",
         "subnet-0850197f",
         "subnet-95bcbdbd"
      ],
      "type": "{{provision}}"
   }
}'''
    if provision=="SPOT":
        template = re.sub(r'{{spot-fleet}}',r'"bidPercentage": 100,"spotIamFleetRole": "arn:aws:iam::080819234838:role/bf-20190603-uswest2-iam-GenomicsEnvBatchSpotFleetR-92EDUPSPAPQ6",',template, flags=re.MULTILINE)
    else:
        template = re.sub(r'{{spot-fleet}}',r'',template, flags=re.MULTILINE)
    template = re.sub(r'{{provision}}',provision,template)
    return template


def get_ceq_template():
    ceq_template='''
{
  "jobQueueName": "{{qname}}",
  "state": "ENABLED",
  "priority": 1000,
  "computeEnvironmentOrder": [
    {
      "order": 1,
      "computeEnvironment": "{{name}}"
    }
  ]
}'''
    return ceq_template

def get_cromwell_template():
    cromwell_template = '''
   {{qname}} {
      actor-factory = "cromwell.backend.impl.aws.AwsBatchBackendLifecycleActorFactory"
      config {
        root = "s3://bf-20190529-uswest2-s3/cromwell-execution"
        auth = "default"
      numSubmitAttempts = 6
      numCreateDefinitionAttempts = 6
        default-runtime-attributes {
           queueArn: "arn:aws:batch:us-west-2:080819234838:job-queue/{{qname}}"
        }
        filesystems {
          s3 {
            auth = "default"
          }
        }
      }
    }

'''
    return cromwell_template

    

if __name__ =='__main__':
    main()
