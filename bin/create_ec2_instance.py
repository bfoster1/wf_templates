#!/usr/bin/env python

import os, sys
import argparse
import boto3
import json
from bson import json_util


def main():
     '''main'''
     parser = argparse.ArgumentParser(description=main.__doc__)
     parser.add_argument("-l", "--launchtemplate", required=False, default="jgi-cromwell-lt-2019102-c58e0a10-fb4b-11e9-98cf-0ac711d65b04", help="launch template\n")
     parser.add_argument("-i", "--instance", required=False, default="r5.4xlarge", help="instance type to launch\n")
     args = parser.parse_args()

     client = boto3.client('ec2')
     response = client.run_instances(
         #default ami for batch (docker running)
         ImageId='ami-0f7bc74af1927e7c8',
         InstanceType=args.instance,
         MaxCount=1,
         MinCount=1,
         KeyName='bf-20190528-west2',
         SecurityGroupIds=[
             'sg-0c53215a6df7253e5',
         ],
         LaunchTemplate=
         {
              'LaunchTemplateName': args.launchtemplate
     },
         TagSpecifications=[
             {
                 'ResourceType': 'instance',
                 'Tags': [
                     {
                         'Key': 'Name',
                         'Value': str(args.launchtemplate)
                     },
                 ]
             },
         ],
         IamInstanceProfile={
             'Arn': 'arn:aws:iam::080819234838:instance-profile/bf-20190603-uswest2-iam-GenomicsEnvBatchInstanceProfile-K3S0YR4V9ZND'
         },
         InstanceInitiatedShutdownBehavior='terminate',
     )

     print(json.dumps(response, indent=3, default=json_util.default))
     return 

if __name__ == '__main__':
    main()
