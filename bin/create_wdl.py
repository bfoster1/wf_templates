#!/usr/bin/env python

import os, sys
import argparse
import glob
import json
import re

def main():
    '''wdl_create: with specified wdl adds backend for destination'''
    wdlpath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","wdl","*.wdl")
    wdls = {os.path.splitext(os.path.basename(pathname))[0] : pathname for pathname in glob.glob(wdlpath)}
    
    
    #comments
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-t", "--template", required=True,choices=sorted(list(wdls.keys())))
    parser.add_argument("-r", "--run_location", required=False,default="aws",choices=['aws','aws-ondemand', 'local', 'jaws'])
    parser.add_argument("-s", "--size", required=False, default="small",choices=['small','med', 'large'])
    parser.add_argument("-o", "--outdir", required=False, default="./")    
    parser.add_argument("--overwrite", required=False,default=False, action='store_true')    
    args = parser.parse_args()
    outfile = os.path.join(args.outdir, args.template + ".wdl")
    if os.path.exists(outfile) and not(args.overwrite):
        sys.stderr.write(outfile + " exists and no --overwrite flag\n")
        sys.exit(255)
    
    backendpath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","wdl","backends.json")
    with open(backendpath, "r")  as f: backends = json.loads(f.read())

    backend_subs = generate_backend_subs(wdls[args.template], args.run_location, args.size, backends)
#    print(json.dumps(backend_subs,indent=3))
    
    with open(outfile, "w") as filehandle:
        filehandle.write(generate_subbed_wdl(wdls[args.template], backend_subs))
    
    return 

def generate_subbed_wdl(wdlpath,substitution_dict):
    '''given an wdl path and substitution dictionary, return wdl with backends substituted'''
    wdl_text = ""
    with open(wdlpath, "r") as filehandle:
        lines = filehandle.readlines()
    current_task = ""
    wdl_doc = list()
    lc = 0
    for line in lines:
        if line.startswith("task "):
            current_task = re.sub(r'task (\w+).*',r'\1',line.rstrip())
            if current_task not in (substitution_dict):
                sys.stderr.write(current_task + " not in " + substitution_dict)
                sys.exit(1)
        if(line.find('Local') !=-1 and line.find('backend')!=-1):
            line = substitution_dict[current_task] + "\n"
        wdl_doc.append(line)
    return "".join(wdl_doc)


def generate_backend_subs(wdlpath,run_location, size, all_backends):
    '''takes a wdl and returns a custom dict with:
    task:backend
    '''
    task_list = get_all_wdl_tasks(wdlpath)
    template = os.path.splitext(os.path.basename(wdlpath))[0]
    backends = dict()
    if run_location.startswith("aws"):
        #set default = all_backends['aws.small']
        backends = {task : all_backends['aws.small'] for task in task_list}
        if template.startswith("metagenome_assembly"):
            if size=="med":
                backends['assy']=all_backends['aws.med']
                backends['read_mapping_pairs']=all_backends['aws.med']
            elif size=="large":
                backends['assy']=all_backends['aws.large']
                backends['read_mapping_pairs']=all_backends['aws.med']
        if run_location.find('demand')!=-1:
            backends = {task : re.sub(r'-ceq',r'D-ceq',backends[task]) for task in backends}
    
    return backends

def get_all_wdl_tasks(wdlpath):
    '''returns a list of tasknames in wdl'''
    tasklist = list()
    with open(wdlpath, "r") as filehandle:
        for line in filehandle:
            if line.startswith("task "):
                task = re.sub(r'task (\w+).*',r'\1',line.rstrip())
                tasklist.append(task)
    return tasklist
    

if __name__ =='__main__':
    main()
    
