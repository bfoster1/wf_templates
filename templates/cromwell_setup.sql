CREATE DATABASE cromwell;
CREATE USER 'testuser'@'localhost' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON cromwell.* TO 'testuser'@'localhost' WITH GRANT OPTION;
CREATE USER 'testuser'@'%' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON cromwell.* TO 'testuser'@'%' WITH GRANT OPTION;
